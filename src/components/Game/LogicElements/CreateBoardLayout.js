let CreateBoardLayout = (elemX, elemY, colorsList)=>{
    let createCurrentStrings = [];
    for (let string = 1; string <= elemX; string++) {
        let createCurrentColumn = [];
        for (let column = 1; column <= elemY; column++) {
            let randomElementInList = (myList) => {
                return( myList[Math.floor(Math.random() * myList.length )] )
            }
            createCurrentColumn.push({color:randomElementInList(colorsList), column, string})
        }
        createCurrentStrings.push(createCurrentColumn)
    }
    return createCurrentStrings
}

export default CreateBoardLayout