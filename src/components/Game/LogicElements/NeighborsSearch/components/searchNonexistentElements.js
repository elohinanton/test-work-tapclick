import colorSearch from "./colorSearch"

let searchNonexistentElements = (board, neighbors)=>{
    let [leftNeighbors, rightNeighbors, topNeighbors, bottomNeighbors] = neighbors
    // опорные данные для поиска краев
    let xMaxLength = board.length - 1
    let yMaxLength = 0;
    for (let i=0; i <= xMaxLength; i++){
        if (board[i].length > yMaxLength-1){yMaxLength = board[i].length-1}
    };

    if(leftNeighbors.x < 0){
        leftNeighbors = NaN
    }else{
        leftNeighbors = {...leftNeighbors, color:colorSearch(board, leftNeighbors)}
    };
    
    if(rightNeighbors.x > xMaxLength+1){
        rightNeighbors = NaN
    }else{
        rightNeighbors = {...rightNeighbors, color:colorSearch(board, rightNeighbors)}
    };
    
    if(bottomNeighbors.y > yMaxLength - 1 ){
        bottomNeighbors = NaN
    }else{
        bottomNeighbors = {...bottomNeighbors, color:colorSearch(board, bottomNeighbors)}
    };
    
    if(topNeighbors.y < 0){
        topNeighbors = NaN
    }else{
        topNeighbors = {...topNeighbors, color:colorSearch(board, topNeighbors)}
    };

    return {left:leftNeighbors, right:rightNeighbors, top:topNeighbors, bottom:bottomNeighbors};
}

export default searchNonexistentElements