let colorSearch = (board, searchElem)=>{
    let line = board[searchElem.y];
    let elem = line[searchElem.x];
    let color = elem.color;
    return (color);
}

export default colorSearch