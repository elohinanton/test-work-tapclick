import searchNonexistentElements from "./components/searchNonexistentElements";


let neighbors = (board, myPos, positions)=>{
    let leftNeighbors = {x:myPos.x - 1, y:myPos.y};
    let rightNeighbors = {x:myPos.x + 1, y:myPos.y};
    let bottomNeighbors = {x:myPos.x, y:myPos.y + 1};
    let topNeighbors = {x:myPos.x, y:myPos.y - 1};

    // if(positions===undefined){
    //     console.log("position undefined");
    // } else {
    //     if (!positions.includes('left')) {
    //         delete leftNeighbors;
    //     }
    //     if (!positions.includes('right')) {
    //         delete rightNeighbors;
    //     }
    //     if (!positions.includes('top')) {
    //         delete topNeighbors;
    //     }
    //     if (!positions.includes('bottom')) {
    //         delete bottomNeighbors;
    //     }
        
    //     // console.log( positions.find('left') );
    //     // console.log( positions.find('right') );
    //     // console.log( positions.find('top') );
    //     // console.log( positions.find('bottom') );
    // }
    
    let elemsInObject = searchNonexistentElements(board, [leftNeighbors, rightNeighbors, topNeighbors, bottomNeighbors] )

    return(elemsInObject)
}

export default neighbors