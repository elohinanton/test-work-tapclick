import { useDispatch, useSelector } from "react-redux"

import './Game.sass'
import CreateBoardLayout from "./LogicElements/CreateBoardLayout"

import GameBoard from "./GameBoard"

function Game(){
  const dispatch = useDispatch()
  const colorsList = useSelector(state=>state.elemColors)
  const boardSize = useSelector(state=>state.boardSize)

  let createBoard = CreateBoardLayout(boardSize.x, boardSize.y, colorsList)
  dispatch({type:"EDIT_ALL_BOARD", payload: createBoard})
  // console.log(createBoard);
  
  return(
    <GameBoard />
  )
  
}

export default Game