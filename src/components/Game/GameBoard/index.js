import { useSelector } from 'react-redux'
import OneElement from './OneElement'


let GameBoard = ()=>{
  let board = useSelector(state=>state.board)
    return(
        <div className="game_board">  
        {
          board.map((string, indexLine)=>(
            <div className="line">
              {string.map((cell, indexCell)=>(
                <OneElement payload={cell} myClass={"game-card__" + cell.string + "-" + cell.column } color={cell.color} />
              ))}
            </div>
          ))
        }
    </div>
    )
}

export default GameBoard