import { useSelector } from "react-redux"
import NeighborsSearch from "../../LogicElements/NeighborsSearch";

function OneElement(props){
    let board = useSelector(state=>state.board)
    
    let myPos = props.myClass.split('__');
    myPos = myPos[1].split('-');
    myPos = {x: (Number(myPos[1])-1), y: (Number(myPos[0])-1), color:props.color};

    function currentCardClick(){
        let delElements = [{...myPos}]
        function neighborsWithSameColor (board, myPos){
            let neighbors = NeighborsSearch(board, myPos, ["left"]);
            let currentBoard = [...board]
            currentBoard.map((yItem)=>{
                yItem.map((xItem)=>{
                    xItem.status = false
                })
            })
            currentBoard[myPos.y][myPos.x].status = true;

            for(let yI = myPos.y-1; yI >= 0; yI--){                
                currentBoard[yI].map((xI)=>{
                    
                    // console.log(NeighborsSearch(board, {x:xI, y:yI} ));
                    
                })
            }
           
            console.log(neighbors);

        }

        neighborsWithSameColor(board,myPos)
    }
    return(
        <div onClick={currentCardClick} className={props.myClass} style={{backgroundColor: props.color}}></div>
    )
}

export default OneElement