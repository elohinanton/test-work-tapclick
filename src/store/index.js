import { createStore } from 'redux';
import { gameParamsReducer } from './gameParamsReducer';

import { composeWithDevTools } from 'redux-devtools-extension';


export const store = createStore(gameParamsReducer, composeWithDevTools())