const defaultState = {
    elemColors: ["red", "green", "blue"],
    boardSize: {x:4, y:5},
    board:[],
}

export const gameParamsReducer = (state = defaultState, action)=>{
    switch(action.type){
      case "EDIT_ALL_BOARD":
        return {...state, board: action.payload }
      case "EDIT_ONE_ELEMENT_BOARD":
        return {...state, }
      default: 
        return state
    }
  }